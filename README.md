<div align="justify">

## Introdução

<b><a target='_blank' rel='noopener noreferrer' href='https://www.r-project.org/'>R</a></b> é um software livre para computação estatística e construção de gráficos. O software é mantido pela R Foundation for Statistical Computing. Trata-se de uma linguagem de programação completa, com orientação a objetos e suporte à interação com outras linguagens. O software R é gratuito, possui livre distribuição e código fonte aberto e é mantido por uma equipe internacional de pesquisadores. O código fonte do R está disponível sob a licença GNU GPL.

Função é um código de programação pré-definido com o objetivo de executar um determinado cálculo e/ou uma tarefa específica. Este repositório contém funções escritas em R disponíveis para download. Para comentários, dúvidas e sugestões entre em contato através do correio eletrônico <statistical.metrology@gmail.com>.

<p> <br /> <p>

## Dados

As bases de dados deste repositório estão armazenadas em arquivos Microsoft &reg; Excel &reg; com extensão xlsx. Os arquivos estão disponíveis para download na pasta <b>[`dados`](Dados)</b>. É importante mencionar que, quando há valores/informações ausentes a célula da planilha eletrônica está "vazia", ou seja, sem nenhum tipo de caracter. Isso é necessário para a correta execução dos códigos em R sugeridos neste respositório. Por fim, para carregar o pacote `xlsx` é necessário que o <a target='_blank' rel='noopener noreferrer' href='https://www.java.com'>Java</a> esteja instalado e atualizado.

<p> <br /> <p>

## 1. Agree tab

A função [`agree.tab`](agree_tab.R) retorna a frequência com que cada avaliador julgou um determinado item. Para a execução da referida função é necessário que o pacote `expss` seja previamente instalado. Clique em <b>[`exemplos`](Exemplos/agree_tab.md)</b> para visualizar uma forma de utilizar a função.

<p> <br /> <p>

## 2. Bplot
 
A função [`bplot`](bplot.R) permite constuir um gráfico de barras para pesquisas de opinião. Para a execução da referida função é necessário que os pacotes `reshape` e `plotly` sejam previamente instalados. É importante destacar que o argumento `label_location` estabelece a posição dos rótulos (percentuais de cada barra). Clique em <b>[`exemplos`](Exemplos/bplot.md)</b> para visualizar uma forma de utilizar a função.

<p> <br /> <p>

## 3. Merge

No arquivo <b>[`exemplos`](Exemplos/merge.md)</b> há um código de programação em R para identificar em um cadastro quais pessoas não responderam a uma pesquisa opinião. De modo análogo, este resultado pode ser obtido utilizando-se o operador <code>%in%</code>. Este operador permite identificar se um ou mais elementos pertencem a um vetor.

```
# Pesquisa de opinião
Populacao = data.frame(Email = paste("email_", 1:9, "@gmail.com", sep = ""))            ; Populacao[,"Email"] = trimws(Populacao[,"Email"]) # População (base cadastral)
Amostra = data.frame(Email = paste("email_", seq(1,9, by = 2), "@gmail.com", sep = "")) ; Amostra[,"Email"] = trimws(Amostra[,"Email"])     # Amostra (respondentes)
ind = Populacao$Email %in% Amostra$Email ; Falta = data.frame(No_Resp = Populacao[which(ind == FALSE),]) # Pessoas da população que NÃO responderam a pesquisa de opinião
Populacao ; Amostra ; Falta

# Operador %in%
c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10) %in% c(5, 9)
```

<p> <br /> <p>

## 4. Erro amostral

Código de programação em R destinado a calcular o erro amostral de pesquisas de opinião. O erro é obtido a partir do tamanho populacional (N) e amostral (n).

```
# Erro amostral
dados = data.frame(Entrevistado = 1:80, P1 = 1:80)
n = nrow(dados) ; n
e = function(N, n, alfa = 0.05) { P = Q = 0.5 ; z = qnorm(1-alfa/2,0,1) ; error = z*sqrt((P*Q/(N-1))*(N/n-1)) ; return(error) }
paste(round(e(N = 700, n)*100, 1), "%", sep = "")
```
<p> <br /> <p>

## 5. Criar menu no R console

Este repositório fornece um arquivo compactado que detalha os procedimentos para criar [`menu`](Criar_menu_no_R_console.zip) no R console. Para executar o exemplo disponível neste repositório é necessário salvar e descompactar o arquivo `zip` no PenDrive `F:`.

<p> <br /> <p>

## 6. Linhas horizontais e verticais (Plotly R)

A função [`hv_line_ly`](hv_line_ly.R) permite construir linhas horizontais e verticais. Acesse [Plotly R](https://plotly.com/r/horizontal-vertical-shapes/).

```
# Carregar função do GitLab
devtools::source_url("https://gitlab.com/luizleal1974/r_functions/raw/main/hv_line_ly.R")

# Plot
library(plotly) ; plot_ly() %>% layout(shapes = list(vline(4), hline(5)))
```

<p> <br /> <p>

## 7. Barras de erro

A função [`error_bars`](error_bars.R) permite constuir um gráfico de barras de erro. Clique em <b>[`exemplos`](Exemplos/error_bars.md)</b> para visualizar uma forma de utilizar a função.

<p> <br /> <p>

## 8. Gráfico de linhas

A função [`line_chart`](line_chart.R) permite constuir um gráfico de linhas para pesquisas de opinião. É importante destacar que o argumento `label_location` estabelece a posição dos rótulos. Clique em <b>[`exemplos`](Exemplos/line_chart.md)</b> para visualizar uma forma de utilizar a função.

<p> <br /> <p>

## 9. Dashboard

Dashboard é um tipo de interface gráfica. No R, o pacote `flexdashboard` permite construção de um dashboard (o qual é salvo em um arquivo HTML). O arquivo [`Flexdashboard`](Flexdashboard.zip) contém um exemplo de dashboard construído a partir do referido pacote. Para informações complementares acesse <a target='_blank' rel='noopener noreferrer' href='https://pkgs.rstudio.com/flexdashboard/'>flexdashboard</a>.

```
# Compilar arquivo Rmd
setwd("F:")
library(rmarkdown)
library(flexdashboard)
render("Flexdashboard.Rmd", output_file = "Apresentação_de_resultados.html")
```

<p> <br /> <p>

## 10. Amostra aleatória estratificada

A função [`n_srs`](n_srs.R) permite realizar o dimensionamento amostral, no caso em que se deseja estima a proporção populacional P, considerando uma amostragem aleatória estratificada com alocação proporcional em que N representa o tamanho populacional, Nh representa o tamanho populacional do h-ésimo estrato, n representa o tamanho amostral e nh representa o tamanho amostral do h-ésimo estrato.

```
# Carregar função do GitLab
devtools::source_url("https://gitlab.com/luizleal1974/r_functions/raw/main/n_srs.R")

# Estratos
dados = data.frame(UF = c("ES", "MG", "RJ", "SP"), Pop = c(3351669, 19273533, 15420450, 39827690))

# Amostra aleatória estratificada
n_srs(strata = dados$UF, Nh = dados$Pop, Ph = NULL, e = 0.02, alfa = 0.05)
```

<p> <br /> <p>

## 11. Options

Função do R que permite definir o formato de apresentação de resultados numéricos.

```
0.123456789 * 10 ^ (-12)
options(digits = 11)     # Define a quantidade de dígitos
0.123456789 * 10 ^ (-12)
options(scipen = 999)    # Remove notação científica
0.123456789 * 10 ^ (-12)
```


<p> <br /> <p>

## 12. Se "erro" retornar ...

Se um determinado comando do R retorna uma mensagem de erro, é possível definir um <i>output</i> específico conforme os códigos sugeridos.

```
# If error function
if_error = function(e, value){suppressWarnings(tryCatch(expr = e, error = function(err){value}))}

# Outputs
if_error(e =    lm(log(0) ~ 1)         , value = "Não é possível estimar"              )
if_error(e =    lm(log(0) ~ 1)         , value = 1                                     )
if_error(e =    lm(log(0) ~ 1)         , value = data.frame(R1 = log(0), R2 = log(1))  )
if_error(e =    lm(log(0) ~ 1)         , value = "NA"                                  )
if_error(e = lm(rnorm(10) ~ runif(10)) , value = "NA"                                  )
```

<p> <br /> <p>

## 13. Suprimir avisos e mensagens 

O R permite suprimir avisos (<i>warnings</i>) e mensagens (<i>messages</i>).

```
suppressWarnings(  expr = library(factoextra)  ) # Suprime aviso.
suppressMessages(  expr = library(factoextra)  ) # Suprime mensagem.
options(warn = -1) ;  as.numeric(c(1, "A", 6)) ; as.numeric(c("B", 7, 9)) # Suprime todos os warnings
options(warn = 0)  ;  as.numeric(c(1, "A", 6)) ; as.numeric(c("B", 7, 9)) # Reativa todos os warnings
```

<p> <br /> <p>

## 14. Se "demorar" interromper a execução da função

É possível interromper uma função cujo tempo de execução exceda, em segundos, um valor pré-fixado. 

```
# Function
f1 = function(x, mu, kappa) { 
 p_von_mises = function(q, mu, kappa) {library(circular, exclude = c( "var", "sd")) ; return(pvonmises(circular(q), circular(mu), kappa))}
  set.seed(1234) ; gof = goftest::ad.test(x, null = p_von_mises, mu , kappa, estimated = TRUE)
  return(gof)
   }

# Data
x = c(1, 3, 9, 12, 26)

# If it takes too long, stop executing the function.
p = c(mu = 2, kappa = 800) ; gof_test_1 = R.utils::withTimeout(expr = {  f1(x, mu = p[1], kappa = p[2])  },
                                                               timeout = 1.5,
                                                               onTimeout = "silent"
                                                               )
p = c(mu = 2, kappa = 8)   ; gof_test_2 = R.utils::withTimeout(expr = {  f1(x, mu = p[1], kappa = p[2])  },
                                                               timeout = 1.5,
                                                               onTimeout = "silent"
                                                               )
# Outputs
gof_test_1
gof_test_2
```

<p> <br /> <p>

## 15. Função indicadora

Tem-se a seguir exemplos de uso da função indicadora no R. Deve-se observar que `TRUE` assume o valor 1 e `FALSE` assume o valor 0.

```
x = -2 ; 3 - (x < 0)
x =  0 ; 3 - (x <= 0)
x =  2 ; 3 - (x < 0)
5 - TRUE
5 - FALSE
```



<p> <br /> <p>

## 16. Instalar versão antiga de pacote

Tem-se a seguir uma sugestão de código para intalar uma versão antiga de um pacote do R. Neste caso, instala-se uma versão antiga do pacote VGAM.

```
# Install old version of R package
packageurl <- "http://cran.r-project.org/src/contrib/Archive/VGAM/VGAM_1.1-6.tar.gz"
install.packages(packageurl, repos = NULL, type = "source")
```


<p> <br /> <p>

</div>

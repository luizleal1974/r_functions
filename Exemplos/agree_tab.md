<div align="justify">

 
## 1. Dados

<p align="center">
<img src="Dados/01_Dados.png" alt="Drawing">
</p>

<p> <br /> <p>

## 2. Codificação

<p align="center">
<img src="Imagens/Escala_Pesquisas.png" alt="Drawing">
</p>

<p> <br /> <p>

## 3. Código

```r
# Carregar função do GitLab
devtools::source_url("https://gitlab.com/luizleal1974/r_functions/raw/main/agree_tab.R")

# Importar arquivo de dados do GitLab
arquivo = tempfile(fileext = ".xlsx")
path = "https://gitlab.com/luizleal1974/r_functions/raw/main/Dados/01_Dados.xlsx"
download.file(path, destfile = arquivo, mode = 'wb')

# Dados
library(xlsx)
dados = read.xlsx(arquivo, sheetIndex = 1, encoding = "UTF-8")

# Frequências
dfr = dados[,2:5]
agree.tab(data = trimws(t(dfr)), categ = c(0:10, "Não sei avaliar"))
dfr = dados[,6:8]
agree.tab(data = trimws(t(dfr)))
```

<p> <br /> <p>

## 4. Saída

<p align="center">
<img src="Output/01_Output.png" alt="Drawing">
</p>

<p> <br /> <p>

</div>

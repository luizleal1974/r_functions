<div align="justify">

## 1. Dados

<p align="center">
<img src="Dados/02_Dados.png" alt="Drawing">
</p>

<p> <br /> <p>


## 2. Código

```r
# Carregar função do GitLab
devtools::source_url("https://gitlab.com/luizleal1974/r_functions/raw/main/error_bars.R")

# Importar arquivo de dados do GitLab
arquivo = tempfile(fileext = ".xlsx")
path = "https://gitlab.com/luizleal1974/r_functions/raw/main/Dados/02_Dados.xlsx"
download.file(path, destfile = arquivo, mode = 'wb')

# Carregar pacotes
library(xlsx)
library(MASS)
library(plotly)

# Dados
dados = read.xlsx(arquivo, sheetIndex = 1, encoding = "UTF-8")
colnames(dados)[-1] = gsub(pattern = "X", replacement = "", colnames(dados)[-1])

# Medidas para o gráfico
xi = apply(X = dados[,-1], MARGIN = 2, FUN = mean, na.rm = TRUE) # Média
s  = apply(X = dados[,-1], MARGIN = 2, FUN = sd,   na.rm = TRUE) # Desvio padrão
Participante = colnames(dados)[-1]                               # Código do participante
dfr = data.frame(Participante, xi, s)                            # NOTA: AS COLUNAS DO DATA FRAME DEVEM TER EXATAMENTE ESSES NOMES

# Valor designado e desvio padrão do EP
xpt   = hubers(xi)$mu
sigma = hubers(xi)$s

# Limites do eixo y
ylim = c(min(c(dfr$xi - dfr$s, xpt - 2*sigma))*0.95, max(c(dfr$xi + dfr$s, xpt + 2*sigma))*1.02)

# Rótulos do 'segundo eixo y'
y_labels = c(paste( "\\LARGE x_{PT} + 2 \\cdot \\sigma_{PT} = " , round(xpt + 2*sigma, 4) , sep = "") ,
             paste( "\\LARGE x_{PT} + \\sigma_{PT} = "          , round(xpt + sigma  , 4) , sep = "") ,
             paste( "\\LARGE x_{PT} = "                         , round(xpt          , 4) , sep = "") ,
             paste( "\\LARGE x_{PT} - \\sigma_{PT} = "          , round(xpt - sigma  , 4) , sep = "") ,
             paste( "\\LARGE x_{PT} - 2 \\cdot \\sigma_{PT} = " , round(xpt - 2*sigma, 4) , sep = "")
             )

# Barras de erro
error_bars(dfr, xpt, sigma, plot_title = "NMHC", xlabel = "Laboratórios"                     ) %>% layout(yaxis = list(range = ylim), margin = list(autoexpand = FALSE, l = 120, r = 100, b = 75, t = 60))
error_bars(dfr, xpt, sigma, plot_title = "NMHC", xlabel = "Laboratórios", y_labels = y_labels) %>% layout(yaxis = list(range = ylim), margin = list(autoexpand = FALSE, l = 120, r = 370, b = 75, t = 60))
```

<p> <br /> <p>

## 3. Saída

<p align="center">
<img src="Output/04_Output.png" alt="Drawing">
</p>

<p> <br /> <p>

</div>

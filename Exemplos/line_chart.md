<div align="justify">

## 1. Dados

<p align="center">
<img src="Dados/03_Dados.png" alt="Drawing">
</p>

<p> <br /> <p>


## 2. Código

```r
# Carregar função do GitLab
devtools::source_url("https://gitlab.com/luizleal1974/r_functions/raw/main/line_chart.R")

# Importar arquivo de dados do GitLab
arquivo = tempfile(fileext = ".xlsx")
path = "https://gitlab.com/luizleal1974/r_functions/raw/main/Dados/03_Dados.xlsx"
download.file(path, destfile = arquivo, mode = 'wb')

# Carregar pacotes
library(xlsx)
library(plotly)

# Dados
dfr = read.xlsx(arquivo, sheetIndex = 1, encoding = "UTF-8")

# Limites do eixo y
ylim = c(min(dfr[,3])*0.92, max(dfr[,3])*1.1)

# Posicionamento dos rótulos
label_location = rep(0, nrow(dfr))
label_location[1] = -0.1
label_location[2] = 0.05
label_location[3] = -0.05
label_location[4] = 0.12
label_location[8] = -0.15

# Gráfico
line_chart(dfr, label_location, ylim, xlabel = "Ano", ylabel = "Nota média") %>%
layout(margin = list(autoexpand = FALSE, l = 100, r = 500, t = 10, b = 50))
```

<p> <br /> <p>

## 3. Saída

<p align="center">
<img src="Output/05_Output.png" alt="Drawing">
</p>

<p> <br /> <p>

</div>

<div align="justify">

## 1. Dados

<p align="center">
<img src="Dados/01_Dados.png" alt="Drawing">
</p>

<p> <br /> <p>

## 2. Codificação

<p align="center">
<img src="Imagens/Escala_Pesquisas.png" alt="Drawing">
</p>

<p> <br /> <p>

## 3. Código

```r
# Carregar função do GitLab
devtools::source_url("https://gitlab.com/luizleal1974/r_functions/raw/main/agree_tab.R")
devtools::source_url("https://gitlab.com/luizleal1974/r_functions/raw/main/bplot.R")

# Importar arquivo de dados do GitLab
arquivo = tempfile(fileext = ".xlsx")
path = "https://gitlab.com/luizleal1974/r_functions/raw/main/Dados/01_Dados.xlsx"
download.file(path, destfile = arquivo, mode = 'wb')

# Dados
library(xlsx)
dados = read.xlsx(arquivo, sheetIndex = 1, encoding = "UTF-8")

# Classes
classes = c("Muito insatisfeito", "Insatisfeito", "Nem satisfeito, nem insatisfeito", "Satisfeito", "Muito satisfeito", "Não sei avaliar")

# Cores
cores = c('#ff3232', '#ff7f7f', '#ffb732', '#b2b2ff', '#6666ff', '#a6a6a6') ; names(cores) = classes

# Selecionar dados
dfr = dados[,2:5]

# Item
Item = c("Ambiente de <br> trabalho", "Instalações", "Suporte de <br> T.I.", "Infraestrutura")

# Frequências
p = agree.tab(data = trimws(t(dfr)), categ = c(0:10, "Não sei avaliar"))

# Gráfico
label_location = rep(0, ncol(dfr)*length(classes))
label_location[17] = -0.5
bplot(data = dfr, p, decimal_places = 1, Item, classes, cores, tamanho = 30, label_location, mean_title = "<b>Média</b>")
```

<p> <br /> <p>

## 4. Saída

<p align="center">
<img src="Output/02_Output.png" alt="Drawing">
</p>

<p> <br /> <p>

</div>

<div align="justify">

## 1. Código

```r
# Dados
dados1 = data.frame(Populacao = LETTERS[1:9], Email = paste("email_", 1:9, "@gmail.com", sep = ""))
dados2 = data.frame(Amostra = seq(1,9, by = 2), Email = paste("email_", seq(1,9, by = 2), "@gmail.com", sep = ""))

# Remover espaços em branco
dados1[,"Email"] = trimws(dados1[,"Email"])
dados2[,"Email"] = trimws(dados2[,"Email"])

# Merge
result = merge(dados1, dados2, by = "Email", all.x = TRUE)

# Pessoas que faltam responder a pesquisa
falta = result[which(is.na(result$Amostra)),] 

# Outputs
dados1 # Base cadastral
dados2 # Responderam a pesquisa
falta  # Faltam responder a pesquisa
```

<p> <br /> <p>

## 2. Saída

<p align="center">
<img src="Output/03_Output.png" alt="Drawing">
</p>

<p> <br /> <p>

</div>
